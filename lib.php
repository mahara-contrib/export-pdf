<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage export-pdf
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

/**
 * PDF export plugin
 *
 * General notes:
 *
 * Returns a .pdf file rather than a zip file like other export plugins
 */
class PluginExportPdf extends PluginExport {

    /**
     * The name of resultant pdf file
     */
    protected $pdffile;

    /**
     * The name of the directory under which all the other directories and 
     * files will be placed in the export
     */
    protected $rootdir;

    /**
     * List of stylesheets to included for the HTML temporary file.
     */
    private $stylesheets = array('' => array());

    /**
    * constructor.  overrides the parent class
    * to set up smarty and the attachment directory
    */
    public function __construct(User $user, $views, $artefacts, $progresscallback=null) {
        global $THEME;
        parent::__construct($user, $views, $artefacts, $progresscallback);

        $this->rootdir = 'portfolio-for-' . self::text_to_path($user->get('username'));
        $directory = "{$this->exportdir}/{$this->rootdir}/";
        if (!check_dir_exists($directory)) {
            throw new SystemException("Couldn't create the temporary export directory $directory");
        }
        $this->pdffile = 'mahara-export-pdf-user'
            . $this->get('user')->get('id') . '-' . $this->exporttime . '.pdf';

        // Find what base stylesheets need to be included
        $themedirs = $THEME->get_path('', true);
        $stylesheets = array('print.css', 'views.css');
        foreach ($themedirs as $theme => $themedir) {
            foreach ($stylesheets as $stylesheet) {
                if (is_readable($themedir . 'style/' . $stylesheet)) {
                    array_unshift($this->stylesheets[''], $themedir . '/style/' . $stylesheet);
                }
            }
        }

        // Don't export the dashboard
        foreach (array_keys($this->views) as $i) {
            if ($this->views[$i]->get('type') == 'dashboard') {
                unset($this->views[$i]);
            }
        }

        $this->exportingoneview = (
            $this->viewexportmode == PluginExport::EXPORT_LIST_OF_VIEWS &&
            $this->artefactexportmode == PluginExport::EXPORT_ARTEFACTS_FOR_VIEWS &&
            count($this->views) == 1
        );

        $this->notify_progress_callback(15, get_string('setupcomplete', 'export'));
    }

    public static function get_title() {
        return get_string('title', 'export.pdf');
    }

    public static function get_description() {
        return get_string('description', 'export.pdf');
    }

    /**
     * Main export routine
     */
    public function export() {
        global $THEME;
        raise_memory_limit('128M');

        $this->notify_progress_callback(90, get_string('creatingpdffile', 'export.pdf'));

        $this->create_html();
        $this->create_pdf();

        $this->notify_progress_callback(100, get_string('Done', 'export'));

        return $this->pdffile;
    }

    public function cleanup() {
        // @todo remove temporary files and directories
    }

    /**
     * Shared template settings for all exported views
     */
    public function get_smarty() {
        $stylesheets = array(
            '<link rel="stylesheet" type="text/css" href="' . get_config('wwwroot') . 'theme/views.css">',
            '<link rel="stylesheet" type="text/css" href="' . get_config('wwwroot') . 'theme/raw/static/style/print.css">'
        );
        $smarty = smarty(
            array(),
            $stylesheets,
            array(),
            array(
                'stylesheets' => array('style/views.css'),
                'sidebars' => false,
            )
        );
        if (get_config('viewmicroheaders')) {
            $smarty->assign('microheaders', true);
        }
        $user = $this->get('user');
        $exporttime = format_date($this->exporttime,'strftimedatetimeshort');
        $smarty->assign('generatedfor', get_string('exportgeneratedfor','export.pdf', display_name($user, null, true), $exporttime, get_config('sitename')));
        return $smarty;
    }

    /**
     * Dumps all views into the PDF ready HTML format
     */
    private function create_html() {
        global $THEME;

        $progressstart = 55;
        $progressend   = 75;
        $i = 0;
        $viewcount = count($this->views);

        // multiple views append header with standard theme
        if (!$this->exportingoneview) {
            $smarty = $this->get_smarty();
            $header = $smarty->fetch('export:pdf:head.tpl');
            if (!file_put_contents("{$this->exportdir}/{$this->rootdir}/"."index.html", $header, FILE_APPEND | LOCK_EX)) {
                throw new SystemException("Could not write view page for pdf export");
            }
        }

        foreach ($this->views as $id => $view) {
            $this->notify_progress_callback(
                intval($progressstart + (++$i / $viewcount) * ($progressend - $progressstart)),
                get_string('exportingviewsprogress', 'export', $i, $viewcount)
            );

            // Set up view theme if we're just exporting a single view
            if (!isset($smarty) && $this->exportingoneview) {
                $viewtheme = $view->get('theme');
                if ($viewtheme && $THEME->basename != $viewtheme) {
                    $THEME = new Theme($viewtheme);
                }
                $smarty = $this->get_smarty();
                $header = $smarty->fetch('export:pdf:head.tpl');
                if (!file_put_contents("{$this->exportdir}/{$this->rootdir}/"."index.html", $header, FILE_APPEND | LOCK_EX)) {
                    throw new SystemException("Could not write view page for pdf export");
                }
            }

            $owner    = $view->get('owner');
            $viewtype = $view->get('type');

            if (get_config('viewmicroheaders')) {
                $smarty->assign('microheadertitle', $view->display_title(true, false));
            }

            if ($viewtype == 'profile') {
                $title = display_name($owner, null, true);
            }
            else {
                $title = $view->get('title');
            }

            if ($viewtype != 'profile' || !get_config('viewmicroheaders')) {
                $smarty->assign('maintitle', hsc($title));
            }

            // fetch the html for a single view
            $smarty->assign('tags', $view->get('tags'));
            $smarty->assign('viewcontent', $view->build_columns());
            $viewcontent = $smarty->fetch('export:pdf:view.tpl');

            // include a pagebreak into the pdf if we are exporting multiple views
            if (!$this->exportingoneview && $i < $viewcount) {
                $viewcontent .= $smarty->fetch('export:pdf:pagebreak.tpl');
            }

            // append any new views to the end of the main html for a bulk export
            if (!file_put_contents("{$this->exportdir}/{$this->rootdir}/"."index.html", $viewcontent, FILE_APPEND | LOCK_EX)) {
                throw new SystemException("Could not write view page for pdf export");
            }
        }
        // append footer
        $footer = $smarty->fetch('export:pdf:foot.tpl');
        if (!file_put_contents("{$this->exportdir}/{$this->rootdir}/"."index.html", $footer, FILE_APPEND | LOCK_EX)) {
            throw new SystemException("Could not write view page for pdf export");
        }
    }

    /**
     * Converts HTML Views into a single PDF document
     */
    private function create_pdf() {
        raise_memory_limit('128M');

        if (is_readable($this->exportdir . $this->rootdir . "/index.html")) {
            $sourcefile = $this->exportdir . $this->rootdir . "/index.html";
        }
        $targetfile = $this->exportdir . $this->pdffile;

        $output = array();
        $wkhtmltopdfpath = get_config_plugin('export','pdf','pathtowkhtmltopdf');
        $xvfbrunpath = get_config_plugin('export','pdf','pathtoxvfbrun');
        $command = sprintf('%s %s %s %s %s',
            $xvfbrunpath,
            $wkhtmltopdfpath,
            '-O landscape',
            $sourcefile,
            $targetfile
        );
        log_debug($command);

        exec($command, $output, $returnvar);
        if ($returnvar != 0) {
            throw new SystemException('Failed to create the pdf file');
        }

        return;
    }

    /**
     * Converts the passed text into a a form that could be used in a URL.
     *
     * @param string $text The text to convert
     * @return string      The converted text
     */
    public static function text_to_path($text) {
        return substr(preg_replace('#[^a-zA-Z0-9_-]+#', '-', $text), 0, 255);
    }

    public static function has_config() {
        return true;
    }

    public static function get_config_options() {
        $elements = array(
            'pathtowkhtmltopdf' => array(
                'type'  => 'text',
                'size' => 50,
                'title' => get_string('pathtowkhtmltopdf', 'export.pdf'),
                'rules' => array(
                    'required' => true,
                ),
                'defaultvalue' => get_config_plugin('export', 'pdf', 'pathtowkhtmltopdf'),
                'help'  => true,
            ),
            'pathtoxvfbrun' => array(
                'type'  => 'text',
                'size' => 50,
                'title' => get_string('pathtoxvfbrun', 'export.pdf'),
                'rules' => array(
                    'required' => true,
                ),
                'defaultvalue' => get_config_plugin('export', 'pdf', 'pathtoxvfbrun'),
                'help'  => true,
            ),
        );

        return array(
            'elements' => $elements,
            'renderer' => 'table'
        );
    }

    public static function validate_config_options($form, $values) {
        $config = array('wkhtmltopdf' => 'pathtowkhtmltopdf', 'xvfb-run' => 'pathtoxvfbrun' );
        // ensure the required software is executable
        foreach ($config as $key => $value) {
            if (!file_exists($values[$value]) || !is_executable(trim(escapeshellcmd($values[$value])))) {
                log_debug("Either you do not have $key installed, or the config setting '$value' is not pointing at your executable.");
                $form->set_error($value, "Either you do not have $key installed, or the config setting '$value' is not pointing at your executable.");
                return false;
            }
        }
    }

    public static function save_config_options($values) {
        $config = array('pathtowkhtmltopdf', 'pathtoxvfbrun' );
        foreach ($config as $config) {
            set_config_plugin('export', 'pdf', $config, $values[$config]);
        }
    }

}

?>
