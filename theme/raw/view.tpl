{if $microheaders}{include file="export:pdf:viewmicroheader.tpl"}{else}{include file="export:pdf:header.tpl"}{/if}

{if $maintitle}<h1>{$maintitle|safe}</h1>{/if}

<p id="view-description">{$viewdescription|clean_html|safe}</p>

<div id="view" class="cb">
        <div id="bottom-pane">
            <div id="column-container">
               {$viewcontent|safe}
                <div class="cb">
                </div>
            </div>
        </div>
</div>
{if $microheaders}{include file="export:pdf:microfooter.tpl"}{else}{include file="export:pdf:footer.tpl"}{/if}
